var valor1, valor2, valorResultado;

function obtenerComponentes() {
    var resultado = document.getElementById("resultado")
    var resetear = document.getElementById("reset")
    var suma = document.getElementById("suma")
    var resta = document.getElementById("resta")
    var multi = document.getElementById("multiplicacion")
    var dividir = document.getElementById("division")
    var igual = document.getElementById("igual")
    var nueve = document.getElementById("nueve")
    var ocho = document.getElementById("ocho")
    var siete = document.getElementById("siete")
    var seis = document.getElementById("seis")
    var cinco = document.getElementById("cinco")
    var cuatro = document.getElementById("cuatro")
    var tres = document.getElementById("tres")
    var dos = document.getElementById("dos")
    var uno = document.getElementById("uno")
    var cero = document.getElementById("cero")
}

//eventos 
uno.onclick = function (e) {
    resultado.textContent = resultado.textContent + "1";
}
dos.onclick = function (e) {
    resultado.textContent = resultado.textContent + "2";
}
tres.onclick = function (e) {
    resultado.textContent = resultado.textContent + "3";
}
cuatro.onclick = function (e) {
    resultado.textContent = resultado.textContent + "4";
}
cinco.onclick = function (e) {
    resultado.textContent = resultado.textContent + "5";
}
seis.onclick = function (e) {
    resultado.textContent = resultado.textContent + "6";
}
siete.onclick = function (e) {
    resultado.textContent = resultado.textContent + "7";
}
ocho.onclick = function (e) {
    resultado.textContent = resultado.textContent + "8";
}
nueve.onclick = function (e) {
    resultado.textContent = resultado.textContent + "9";
}
cero.onclick = function (e) {
    resultado.textContent = resultado.textContent + "0";
}
reset.onclick = function (e) {
    resetear();
}
suma.onclick = function (e) {
    valor1 = resultado.textContent;
    resultado.textContent += " + "
    console.log(valor1)
    valorResultado = " + ";

}
resta.onclick = function (e) {
    valor1 = resultado.textContent;
    resultado.textContent += " - "
    console.log(valor1)
    valorResultado = " - ";

}
multiplicacion.onclick = function (e) {
    valor1 = resultado.textContent;
    resultado.textContent += " x "
    valorResultado = " x ";

}
division.onclick = function (e) {
    valor1 = resultado.textContent;
    resultado.textContent += " / "
    valorResultado = " / ";

}
igual.onclick = function (e) {
    operaciones();
}

function limpiar() {
    resultado.textContent = "";
}
function resetear() {
    resultado.textContent = "";
    valor1 = 0;
    valor2 = 0;
    valorResultado = "";
}

function operaciones() {
    var res = 0;
    switch (valorResultado) {
        case " + ":
            var i = resultado.textContent
            i = i.split(" + ");
            valor2 = i[1]
            console.log(valor2)
            res = parseFloat(valor1) + parseFloat(valor2);
            break;
        case "-":
            var i = resultado.textContent
            i = i.split(" - ");
            valor2 = i[1]
            console.log(valor2)
            res = parseFloat(valor1) - parseFloat(valor2);
            break;
        case " x ":
            var i = resultado.textContent
            i = i.split(" x ");
            valor2 = i[1]
            res = parseFloat(valor1) * parseFloat(valor2);
            break;
        case " / ":
            var i = resultado.textContent
            i = i.split(" / ");
            valor2 = i[1]
            res = parseFloat(valor1) / parseFloat(valor2);
            break;
    }
    resetear();
    resultado.textContent = res;
}
