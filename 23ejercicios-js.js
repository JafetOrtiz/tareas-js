function f(x, y = 2, z = 7) { //crea una funcion llamada f a la cual le asigna 3 parametros: "x" sin valor, "y" con una valor de 2 
    //y "z" con un valor de 7
    return x + y + z //devuelve un valor igual a la suma de nuestros parametro "x","y","z"
}
console.log(f(5, undefined)) //se invoca a la funcion f dandole un valor al parametro x de 5, el resultado se vera en consola


var animal = "kitty"
//se decalara una variable llamada animal con una valor de tipo string "kitty"
var resultado = (animal === "kitty") ? "cute" : "still nice"
//se declara una variable llamada cuyo valor sera igual a la condicion
//de si la variable animal es estrictamente igual al valor kitty, 
//si es un tipo string y lleva la palabra kitty imprimira el string "cute"
//si no se cumple la condicion imprimira el string "still nice"


var animal = "kitty" //se declara una variable llamada animal de tipo string con un valor de "kitty"
var result = "" //se decalara una variable llamada result de tipo string con una valor "vacion"
if (animal === "kitty") { //se declara un if con la siguiente condicion: si la variable animal es estrictamente "kitty" entonces
    result = "cute" //si la condicion es verdadera a la variable resultado se le asigna la palabra "cute"
}
else { // se declara un else que complementar la condicion if
    result = "still nice" //si la condicion no se cumple a la variable resultado se le signa la palabra "still nice"
}

var a = 0 //se declara una variable llamada a de tipo int = con un valor de cero
var str = 'not a' //se declara una variable llamada str de tipo string con la palabra "not a"
var b = "" //se declara un variable llamada b de tipo string sin palabra asignada
b = a === 0 ? (a = 1, str += "test") : (a = 2) //la variable b omara un nuevo valor dependiendo de la siguiente condicion
// si a es estrictamente igual a 0 entonces a la variable "a" se le asignara el valor de 1 y a la variable str se le sumara la palabra test
//si no se cumpple la condicion a la variable a se le asigna el valor de 2 

var a = 1 //se declara una variable a tipo int con una valor de 1
a === 1 ? alert("Hey, it is 1!") : 0 // se decalara una condicion que dice que si la variable "a" es estrictamente igual a 1
//entonces se mostrara una ventana de alerta con la frase "hey, it is 1!"
//si no se cumple la condicion se asigna un cero

a === 1 ? alert("Hey, it is 1!") : alert("Weird, what could it be?")
//se declara una condicion que dice:
//si a es estrictamente igual a 1 entonces aparecera una ventana de alerta en pantalla con la frase "hey, it si 1! "
//si no se cumple entonces aparecera una de alerta en pantalla con la frase "Weird, what couls it be?"

if (a === 1) alert("Hey, it is 1")  //se declara una condicion if que dice:
// si la variable a es estrictamente igual a 1 entonces aparecera una ventana de alerta en pantalla con la frase "hey, it si 1! "
else alert("Weird, what could it be?") //se declara un else que indica:
//si no se cumple entonces aparecera una de alerta en pantalla con la frase "Weird, what couls it be?"

var animal = "kitty" //se declara un variable llamada animal con la palabra "kitty" como valor
for (var i = 0; i < 5; i++) { //se declara un ciclo for con lo siguiente:
    //una variable inicializadora llamada i con un valor de cero
    //la condicion que dice que i debe ser menor a 5
    //un contador que incrementa su valor en 1
    (animal === "kitty") ? break: console.log(i)
    //se declara un operardor ternario que nos dice: 
    //si la variable animal es estrictamente igual a la palabra "kitty"
    //entonces se hara un interrupcion que imprimir en consola a la variable i 
}

var value = 1 //se declara un variable llamada value de tipo int con un valor de 1
switch (value) { //se declara una estructura para casos switch que tiene como parametro a la variable value
    case 1: // se declara el primer caso llamado 1
        console.log("I will always run") //si la variable value tiene un valor de 1 entonces
        //se mostrara en consola la frase "i will always run"
        break //se declara una interrupcion para indicar que el caso 1 termina 
    case 2: // se declara el primer caso llamado 1
        console.log("I will never run")//si la variable value tiene un valor de 2 entonces 
        //se mostrara en consola la frase "i will never run"
        break //se declara una interrupcion para indicar que el caso 2 termina
}

var animal = "Lion" //se declara una variable llamada animal de tipo string con la palabra "Lion" como valor
switch (animal) { //se declara una estructura para casos switch que tiene como parametro a la variable animal
    case "Dog": //se declara el caso "Dog"
        console.log('I will not run since animal !=="Dog"') //si se selecciona el caso "Dog" se imprimira en consola "I will not run since animal !==no es un Dog"
        break //se declara una interrupcion para indicar que el caso "Dog" termina
    case "Cat": //se declara el caso "Cat"
        console.log('I will not run since animal !=="Cat"') //si se selecciona el caso "Cat" se imprimira en consola "I will not run since animal !==no es un Cat"
        break //se declara una interrupcion para indicar que el caso "Cat" termina
    default: //se declara un default que se aplica cundo ningun caso es seleccionado 
        console.log("I will run since animal does not match any other case") //se imprime en consola "I will run since animal does not match any other case"
}


function john() { //se declara un funcion llamada john
    return "John" //la funcion retornara el string "John"
}
function jacob() { //se declara una funcion llamada jacob
    return "Jacob" //la funcion retornara el string "John"
}


switch (name) { //se declara una estructura para casos switch que tiene como parametro name
    case john(): //se declara el primer caso cuyo valor sera la funcion john()
        console.log('I will run if name === "John"') //se imprime en consola la frase "'I will run if name === "John"'"
        break //se declara un break para terminar el primer caso
    case 'Ja' + 'ne': //se declara el segundo caso cuyo valor es una concatenacion de los strings Ja + ne
        console.log('I will run if name === "Jane"')//se imprimime en consola la frase 'I will run if name === "Jane"'
        break //se declara un break para terminar el tercer caso 
    case john() + "" + jacob() + " Jingleheimer Schimidt"://se declara un ultimo caso cuyo valor es una concatenacion entre la funcion john() un espacio la funcion jacob() y el string " Jingleheimer Schimidt"
        console.log("His name is equal to name too!")//see imprime en consola la frase "His name is equal to name too!"
        break //se declara un un brak para terminar el ultimo caso
}

var x = "c"
switch (x) {
    case "a":
    case "b":
    case "c":
        console.log("Either a, b, or c was selected")
        break
    case "d":
        console.log("Only d was selected.")
        break
    default:
        console.log("No case was matched.")
        break
}

var x = 5 + 7
var x = 5 + "7"
var x = "5" + 7
var x = 5 - 7
var x = 5 - "7"
var x = "5" - 7
var x = 5 - "x"

var a = "hello" || ""
var b = "" || []
var c = "" || undefined
var d = 1 || 5
var e = 0 || {}
var f = 0 || "" || 5
var g = "" || "yay" || "boo"

var a = "hello" && ""
var b = "" && []
var c = undefined && 0
var d = 1 && 5
var e = 0 && {}
var f = "hi" && [] && "done"
var g = "bye" && undefined && "adios"

var foo = function (val) {
    return val || "default"
}
console.log(foo("burger"))
console.log(foo(100))
console.log(foo([]))
console.log(foo(0))
console.log(foo(undefined))


var isLegal = age >= 18
var tall = height >= 5.11
var suitable = isLegal && tall
var isRoyalty = status === "royalty"
var specialCase = isRoyalty && hasInvitation
var canEnterOurBar = suitable || specialCase


for (var i = 0; i < 3; i++) {
    if (i === 1) {
        continue;
    }
    console.log(i)
}

var i = 0
while (i < 3) {
    if (i === 1) {
        i = 2
        continue
    }
    console.log(i)
    i++
}

for (var i = 0; i < 5; i++) {
    nextLoop2Iteration:
    for (var j = 0; j < 5; j++) {
        if (i == j) break nextLoop2Iteration
        console.log(i, j)
    }
}

function foo() {
    var a = "hello"

    function bar() {
        var b = "world"
        console.log(a)
        console.log(b)
    }
    console.log(a)
    console.log(b)
}
console.log(a)
console.log(b)

function foo() {
    const a = true

    function bar() {
        const a = false
        console.log(a)
    }
    const a = false
    a = false
    console.log(a)
}

var namedSum = function sum(a, b) {
    return a + b
}
var anonSum = function (a, b) {
    return a + b
}
namedSum(7, 3)
anonSum(3, 3)


let decirNombre = function (obj) {
    obj.hablar = function () {
        console.log(this.nombre)
    }
}

